
export default {
    database: {
        hostName: "history-db-service",
        host: "history-db-service",
        port: "27017",
        user: "mongoadmin",
        pwd: "secret",
        db: "admin"
    },
}