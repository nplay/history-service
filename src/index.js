const { ServiceBroker } = require("moleculer");
const E = require("moleculer-web").Errors;

import {Config} from "core"
import V1Service from './V1/service'

global.broker = new ServiceBroker({
    nodeID: process.env.HOSTNAME,
    transporter: Config.queue.driver.rabbitMQ.connectionString
});

let fnRun = async () => {

    await V1Service(broker)

    // Start server
    await broker.start();
}

fnRun()