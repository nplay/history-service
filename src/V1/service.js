
import GeolocationService from './geolocation/service'
import WeatherService from './weather/service'
import HistoryService from './history/service'
import APIService from './api/service'

export default async (broker) => {
    broker.createService(APIService);

    broker.createService(GeolocationService);
    
    broker.createService(WeatherService);
    broker.createService(HistoryService);
}