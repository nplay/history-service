
import getAction from '../method/get'

export default {
    params: {
        ip: { type: "string"}
    },
    async handler(ctx){
        return await getAction(
            {
                ip: ctx.params.ip
            }
        )
    }
}