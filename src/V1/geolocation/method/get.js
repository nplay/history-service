
import axios from 'axios'
import GeolocationModel from '../../../model/geoLocation'

class GeolocationMethod
{
    constructor(ip)
    {
        this.ip = ip
    }

    async find()
    {
        let timeValid = new Date()
        timeValid = timeValid.setHours(timeValid.getHours() -6)

        let geoLocation = await GeolocationModel().findOne({
            created_at: {
                $gte: timeValid
            },
            ip: this.ip
        })

        if(!geoLocation)
            geoLocation = await this.newGeolocation()

        return geoLocation
    }

    async newGeolocation()
    {
        let data = await this.requestGeoLocation()
        return await GeolocationModel().create(data)
    }

    async requestGeoLocation()
    {
        let response = null

        try {
            response = await axios.get(`https://ipinfo.io/${this.ip}`, {
                params: {
                    token: 'b02e8e2cb83b3e'
                },
                headers: {
                    'Accept':  'application/json'
                }
            });
        } catch (error) {
            throw new Error(`Fail in retrive IP Data \n: ${error.toString()}`)
        }

        let body = response.data
        let loc = body.loc.split(',')

        return {
            ip: body.ip,
            country: null,
            countryCode: body.country,
            city: body.city,
            region: await this._stateInitial(body.region),
            lat: loc[0],
            long: loc[1]
        }
    }

    async _stateInitial(fullName)
    {
        let states = {
            'Paraíba': 'PB',
            'Paraná': 'PR',
            'Pernambuco': 'PE',
            'Piauí': 'PI',
            'Rio de Janeiro': 'RJ',
            'Rio Grande do Norte': 'RN',
            'Rio Grande do Sul': 'RS',
            'Rondônia': 'RO',
            'Roraima': 'RR',
            'Santa Catarina': 'SC',
            'São Paulo': 'SP',
            'Sergipe': 'SE',
            'Tocantins': 'TO',
            'Acre': 'AC',
            'Alagoas': 'AL',
            'Amapá': 'AP',
            'Amazonas': 'AM',
            'Bahia ': 'BA',
            'Ceará': 'CE',
            'Distrito Federal ': 'DF',
            'Espírito Santo': 'ES',
            'Goiás': 'GO',
            'Maranhão': 'MA',
            'Mato Grosso': 'MT',
            'Mato Grosso do Sul': 'MS',
            'Minas Gerais': 'MG',
            'Pará': 'PA'
        }

        let state = states[fullName]

        if(!state)
            throw new Error(`Retrive stateInitial failed`)

        return state
    }
}

export default async ({
    ip
}) => {
    if(!ip)
        throw new Error(`IP not found impossible continue geolocation`)
    
    let geoMethod = new GeolocationMethod(ip)
    return await geoMethod.find()
}