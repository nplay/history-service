
import byIPAction from './action/byIP'

export default {
    name: "geolocation",
    version: 1,
    methods: {
    },
    actions: {
        byIP: byIPAction
    }
}