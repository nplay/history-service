
const fetch = require('node-fetch');

import WeatherModel from '../../../model/weather'

class Weather
{
    constructor()
    {
        this.host = 'http://api.openweathermap.org/data/2.5/weather'
        this.appId = '54fdc23d9fa7f52a0ea36ef7b4ebbbe7'
    }

    async request({
        city,
        state,
        countryCode
    })
    {
        let response = await fetch(`${this.host}?q=${city},${state},${countryCode}&units=metric&appid=${this.appId}`)

        if(response.status != 200)
            throw new Error(`Fail in retrive Weather Data`)

        let body = await response.json()

        return {
            date: new Date(),
            city: city,
            region: state,
            countryCode: countryCode,

            temp: body.main.temp,
            climate: this._climate(body.weather[0].id)
        }
    }

    async byRegion(region = {
        city,
        state,
        countryCode
    }, gteDate = null){

        let query = {
            city: region.city,
            region: region.state,
            countryCode: region.countryCode
        }

        if(gteDate != null)
            query.date = { $gte: gteDate }

        let weather = await WeatherModel().findOne(query)

        if(!weather)
        {
            console.log('Novo')
            
            let data = await this.request(region)
            weather = await WeatherModel()(data).save()
        }

        return weather
    }

    _climate(weatherId)
    {
        let conditions = [
            (id) => { return id >= 200 && id <= 232 ? 'tempestade' : false },
            (id) => { return id >= 300 && id <= 321 ? 'chuvisco' : false },
            (id) => { return id >= 500 && id <= 531 ? 'chuva' : false },
            (id) => { return id >= 600 && id <= 622 ? 'neve' : false },
            (id) => { return id >= 701 && id <= 781 ? 'atmosfera' : false },
            (id) => { return id == 800 ? 'limpo' : false },
            (id) => { return id >= 801 && id <= 804 ? 'nuvens' : false }
        ]

        let result = null

        for(let condition of conditions)
        {
            result = condition(weatherId)

            if(result)
                break
        }

        return result
    }
}

export {
    Weather
}