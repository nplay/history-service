
import getByIP from './action/getByIP'
import getByRegion from './action/getByRegion';

export default {
    name: "weather",
    version: 1,
    methods: {
    },
    actions: {
        byIP: getByIP,
        byRegion: getByRegion
    }
}