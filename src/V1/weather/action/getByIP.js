
import weatherMethod from '../method/weather'

export default {
    params: {
        ip: { type: "string"},
        gteDate: {
            type: "string",
            optional: true,
            default: null
        }
    },
    async handler(ctx){
        let ip = ctx.params.ip
        let gteDate = ctx.params.gteDate

        let geoData = await broker.call('v1.geolocation.byIP', {
            ip: ip
        })

        if(!geoData)
            throw new Error(`Geolocation not found impossible retrive weather`)

        let region = {
            city: geoData.city,
            state: geoData.region,
            countryCode: geoData.countryCode
        }

        return await broker.call('v1.weather.byRegion', {
            region: region,
            gteDate: gteDate
        })
    }
}