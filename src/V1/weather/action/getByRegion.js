
import { Weather } from '../method/weather'

export default {
    params: {
        region: {
            type: "object",
            props: {
                city: { type: "string"},
                state: { type: "string"},
                countryCode: { type: "string"}
            }
        },
        gteDate: {
            type: "string",
            optional: true,
            default: null
        }
    },
    async handler(ctx){
        let weather = new Weather()
        let climate = await weather.byRegion(ctx.params.region, ctx.params.gteDate)

        return climate
    }
}