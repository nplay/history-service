
const E = require("moleculer-web").Errors;
const ApiService = require("moleculer-web");
ApiService.settings.port = 3004
const Event2 = require("event2");

let onBeforeCall = function(ctx, route, req, res) {
    // Set request headers to context meta
    ctx.meta.headers = req.headers;
    ctx.meta.req = req
}

let onAfterCall = function(ctx, route, req, res, data) {

    let responseData = {
        success: true,
        data: data
    }

    return responseData
}

let bodyParsers = { json: true }

export default {
    mixins: [ApiService, Event2],
    settings: {
        cors: {
            // Configures the Access-Control-Allow-Origin CORS header.
            origin: "*",
            // Configures the Access-Control-Allow-Methods CORS header. 
            methods: ["GET", "OPTIONS", "POST", "PUT", "DELETE", "PATCH"],
            // Configures the Access-Control-Allow-Headers CORS header.
            //allowedHeaders: [],
        },

        routes: [

            {
                path: "/v1",
                authorization: true,
    
                // Action aliases
                aliases: {
                    "PATCH get": "v1.history.update",
                },

                onBeforeCall,
                onAfterCall(ctx, route, req, res, historyModel) {

                    let responseData = {
                        success: true,
                        data: {
                            sessionID: historyModel.sessionID,
                            user: historyModel.user || null
                        }
                    }
                    
                   this.emit2(ctx, 'v1.history.update-apiBefore', {
                        history: historyModel,
                        identifier: req.$params.identifier,
                        headers: ctx.meta.headers || {}
                    })
                
                    return responseData
                },
                bodyParsers: bodyParsers,
            },

            {
                path: "/v1",
                authorization: true,
                aliases: {
                    "POST weather/byIP": "v1.weather.byIP",
                    "POST weather/byRegion": "v1.weather.byRegion",
                },

                onBeforeCall,
                onAfterCall,
                bodyParsers: bodyParsers,
            },

            {
                path: "/v1",
                authorization: true,
                aliases: {
                    "POST insert": "v1.history.insert",
                },

                onBeforeCall,
                onAfterCall(ctx, route, req, res, data) {

                    let responseData = {
                        success: true,
                        sessionID: data.sessionID
                    }
                
                    return responseData
                },
                bodyParsers: bodyParsers,
            },
        ],

        onError(req, res, err) {
                    
            let responseData = {
                success: false,
                data: {
                    message: err.message
                }
            }

            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.end(JSON.stringify(responseData));
        },
    },
    methods: {
        async authorize(ctx, route, req, res) {

            // Read the token from header
            let token = req.headers["authorization-token"];

            if(!token)
                return Promise.reject(new E.UnAuthorizedError(E.ERR_NO_TOKEN));

            let response = await broker.call('v1.auth.validate', {token: token});

            if(response.body.success == false || response.body.allowed == false)
                return Promise.reject(new E.UnAuthorizedError(E.ERR_INVALID_TOKEN));
            
            let tokenPayload = JSON.parse(response.body.tokenPayload)
            req.$params.identifier = tokenPayload.dId

            return Promise.resolve(ctx);
        }
    }
}