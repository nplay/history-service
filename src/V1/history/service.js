
import HistoryModel from '../../model/history'
import connectionMethod from '../../method/connection'
import ItemHoursMethod from './action/insert/method/itemHours'

import getAction from './action/get'
import updateAction from './action/update'

import setIpListener from './listener/setIpListener'
import setGeolocationListener from './listener/setGeolocationListener'
import setWeatherListener from './listener/setWeatherListener'

import userAgentListener from './listener/userAgent'

const Event2 = require("event2");

export default {
    name: "history",
    version: 1,
    mixins: [Event2],
    methods: {

        connectionMethod,

        async addItem(historyModel, item)
        {
            let exists = historyModel.itens.some(histItem => {

                let found = histItem.product.entityId == item.product.entityId

                if(!found)
                    return false
                
                ++histItem.views;
                histItem = Object.assign(histItem, item)
                
                return found
            });

            if(!exists)
                historyModel.itens.push(Object.assign({}, item))
        },

        async addProspectItem(historyModel, item)
        {
            let exists = historyModel.prospectItens.some(histItem => {
                let found = histItem.product.entityId == item.product.entityId
                let prospected = histItem.orderId != null

                if(!found || prospected)
                    return false
                
                ++histItem.views;
                histItem = Object.assign(histItem, item)
                
                return found
            });

            if(!exists)
                historyModel.prospectItens.push(Object.assign({}, item))
        }
    },
    actions: {

        update: updateAction,

        get: getAction,

        async insert(ctx) {
            try {
                let params = Object.assign({}, ctx.params)

                let identifier = params.identifier
    
                let visualizedItens = params.itens
                let prospectItens = params.prospectItens
                let sessionID = params.sessionID
    
                sessionID = sessionID == "null" || sessionID == "undefined" ? null : sessionID
    
                let historyModel = await broker.call('v1.history.get', {
                    sessionID: sessionID,
                    identifier: params.identifier
                })
    
                if(historyModel)
                {
                    for(let item of visualizedItens)
                        await this.addItem(historyModel, item)
    
                    for(let item of prospectItens)
                        await this.addProspectItem(historyModel, item)
                
                    await historyModel.save()
    
                    let data = Object.assign(params, historyModel.toObject())

                    historyModel = await broker.call('v1.history.update', {
                        identifier: identifier,
                        sessionID: sessionID,
                        data: data
                    })
                }
                else
                {
                    historyModel = await broker.call('v1.history.update', {
                        identifier: identifier,
                        sessionID: sessionID,
                        data: params
                    })
                }
    
                let beforeSave = async () => {
                    await ItemHoursMethod(
                        ctx.params.itens,
                        ctx.params.prospectItens,
                        historyModel
                    )
                }
    
                beforeSave()
    
                return historyModel
            } catch (error) {
                console.log(error)
            }
        },

        async bestViewed(ctx){
            let conn = await this.connectionMethod(ctx)

            let categoryIds = params.categoryIds
            let itensId = params.itensId

            return await HistoryModel(conn).bestViewed(categoryIds, itensId)
        }
    },
    events: {
        "analytics.savedBefore": {
            async handler(ctx) {

                let conn = await this.connectionMethod(ctx)

                let identifier = ctx.params.identifier
                let sessionID = ctx.params.sessionID
                let order = ctx.params.order

                let history = await HistoryModel(conn).findOne({ sessionID: sessionID }, { prospectItens: 1})

                if(!history)
                    throw new Error(`History session not found: ${sessionID} `);
        
                for(let idx in order.items)
                {
                    let item = order.items[idx];
        
                    let historyIdx = history.prospectItens.findIndex((obj) => {
                        let validItem = obj.product.entityId == item.entityId || obj.product.id == item.id
                        let notProspected = obj.orderId == null

                        return validItem && notProspected
                    })
        
                    if(historyIdx == -1)
                        continue;
        
                    let historyItem = history.prospectItens[historyIdx]
        
                    order.items[idx].originCollection = historyItem.originCollection
                    history.prospectItens[historyIdx].orderId = order.identification
                    ++idx
                }

                await history.save()

                //Call Service Analytics (Switch to queue future)
                broker.call('v1.analytics.update', {
                    order: order,
                    identifier: identifier
                })
            }
        }
    },
    events2: [{
        event: 'v1.history.updateAfter',
        listeners: {
            userAgent: {
                handle: userAgentListener
            },
            setIp: {
                handle: setIpListener
            },
            setGeolocation: {
                handle: setGeolocationListener
            },
            setWeather: {
                handle: setWeatherListener
            }
        }
    }]
}