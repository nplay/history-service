
import UserAgentMethod from '../method/userAgent'

export default async function(content, message, ok, fail){
    try {
        let history = content.params.history
        let sessionID = history.sessionID
        let userAgent = content.params.headers?.['user-agent']

        let messageWarn = null

        if(!sessionID)
            messageWarn = `sessionID empty impossible continue weather to history`
        else if(!content.params.identifier)
            messageWarn = `identifier empty impossible continue weather to history`

        if(messageWarn)
        {
            console.warn(messageWarn)
            return ok(message)
        }

        let historyModel = await broker.call('v1.history.get', {
            sessionID: sessionID,
            identifier: content.params.identifier
        })

        await UserAgentMethod(userAgent, historyModel)

        ok(message)
    } catch (error) {
        console.log(error)
        fail(message)
    }
}