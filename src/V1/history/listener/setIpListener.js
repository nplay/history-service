

import setIp from '../method/setIp'

export default async function(content, message, ok, fail){
    try {
        let history = content.params.history
        let sessionID = history.sessionID
        let ip = content.params.headers?.['x-forwarded-for']

        let messageWarn = null

        if(!sessionID)
            messageWarn = `sessionID empty impossible continue set IP to history`
        else if(!content.params.identifier)
            messageWarn = `identifier empty impossible continue set IP to history`
        else if(!ip)
            messageWarn = `ip empty impossible continue set IP to history`

        if(messageWarn)
        {
            console.warn(messageWarn)
            return ok(message)
        }

        let historyModel = await broker.call('v1.history.get', {
            sessionID: sessionID,
            identifier: content.params.identifier
        })

        await setIp(ip, historyModel)

        ok(message)
    } catch (error) {
        console.log(error)
        fail(message)
    }
}