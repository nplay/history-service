
import geolocationMethod from '../method/geolocation'

export default async function(content, message, ok, fail){
    try {
        let history = content.params.history
        let sessionID = history.sessionID

        let messageWarn = null

        if(!sessionID)
            messageWarn = `sessionID empty impossible continue weather to history`
        else if(!content.params.identifier)
            messageWarn = `identifier empty impossible continue weather to history`
        else if(!history)
            messageWarn = `history empty impossible continue weather to history`

        if(messageWarn)
        {
            console.warn(messageWarn)
            return ok(message)
        }

        await geolocationMethod({
            historyModel: history,
            identifier: content.params.identifier
        })

        ok(message)
    } catch (error) {
        console.log(error)
        fail(message)
    }
}