
import weather from '../method/weather'

export default async function(content, message, ok, fail){
    try {
        let history = content.params.history
        let sessionID = history.sessionID
        let ip = history.ip

        let messageWarning = null

        if(!sessionID)
            messageWarning = `sessionID empty impossible continue weather to history`
        else if(!content.params.identifier)
            messageWarning = `identifier empty impossible continue weather to history`
        else if(!ip)
            messageWarning = `ip empty impossible continue weather to history`

        if(messageWarning){
            console.warn(messageWarning)
            return ok(message)
        }

        await weather({
            historyModel: history,
            identifier: content.params.identifier
        })

        ok(message)
    } catch (error) {
        console.log('ERRO HISTORY UPDATE', error)
        fail(message)
    }
}