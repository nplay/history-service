
import getHistory from '../method/getHistory'
import connection from '../../../method/connection'

export default {
    async handler(ctx){
        let conn = await connection(ctx)
        
        let identifier = ctx.params.identifier
        let historyModel = await getHistory(Object.assign({}, ctx.params), conn, ctx)

        this.emit2(ctx, 'v1.history.get', {
            history: historyModel,
            identifier: identifier,
            headers: ctx.meta.headers || {}
        })

        return historyModel
    }
}