
class Wrapper 
{
    constructor(historyModel)
    {
        this.historyModel = historyModel
    }

    async check(item)
    {
        let access = await this.accessLastHour(item)

        if(!access)
            access = await this.createAccess(item)
        else
            ++access.ocurrence
    }

    async accessLastHour(item)
    {
        //Every 1 hour
        let hourTime = 3600000
        let lastHour = Date.now() - hourTime

        let idx = item.accessCollection.length -1
        let lastAccess = item.accessCollection[idx]

        if(!lastAccess)
            return null

        let accessTime = new Date(lastAccess.date).getTime()
        return accessTime < lastHour ? null : lastAccess
    }

    async createAccess(item)
    {
        let data = {
            date: new Date(),
            ocurrence: 1
        }

        let idx = 1 - item.accessCollection.push(data)
        return item.accessCollection[idx]
    }
}

export default async (visualizedItens, prospectItens, historyModel) => {

    let wrapper = new Wrapper(historyModel)

    for(let _item of visualizedItens)
    {
        let item = historyModel.itens.find(item => { return item.product.entityId == _item.product.entityId })
        if(item)
            await wrapper.check(item)
    }

    for(let _item of prospectItens)
    {
        let item = historyModel.prospectItens.find(item => { return item.product.entityId == _item.product.entityId })
        if(item)
            await wrapper.check(item)
    }
    
    await historyModel.save()
    return historyModel
}