import updateHistory from "../method/updateHistory"

export default {
    params: {
        identifier: {type: "string"},
        sessionID: {type: "string", optional: true, default: null},
        data: {
            type: "object",
            strict: false,
            optional: true,
            props: {
                user: {
                    type: "object",
                    strict: true,
                    optional: true,
                    params: {
                        _id: { type: "any", optional: true },
                        sId: { type: "string" },
                        email: { type: "string" },
                        name: { type: "string" }
                    }
                }
            }
        }
    },
    async handler(ctx){

        let sessionID = ctx.params.sessionID
        sessionID = sessionID == 'undefined' ? null : sessionID
        let data = ctx.params || {}
        let identifier = ctx.params.identifier

        let historyModel = await updateHistory({
            sessionID: sessionID,
            identifier: identifier,
            data: data
        })

       this.emit2(ctx, 'v1.history.updateAfter', {
            history: historyModel,
            identifier: identifier,
            headers: ctx.meta.headers || {}
        })
        
        return historyModel
    }
}