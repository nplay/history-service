import Conector from '../../../conector';

import HistoryModel from '../../../model/history'

export default async ({
    historyModel,
    identifier
}) => {
    try
    {
        if(!historyModel.ip)
        {
            console.warn(`IP not found impossible continue`)
            return null
        }
            
        let geolocation = null
        
        let timeValid = new Date()
        timeValid = timeValid.setHours(timeValid.getHours() -6)

        //Find valid and updated geolocation
        for(let _geolocation of historyModel.geolocation)
        {
            let time = new Date( _geolocation.date ).getTime()
            let valid = time >= timeValid && _geolocation.ip == historyModel.ip

            if(!valid)
                continue

            geolocation = geolocation || _geolocation
            let timeGeolocation = new Date(geolocation.date).getTime()

            geolocation = timeGeolocation <= time ? _geolocation : geolocation
        }

        if(geolocation)
            return geolocation

        let geoInfo = await broker.call('v1.geolocation.byIP', {
            ip: historyModel.ip,
            identifier: identifier
        })

        geolocation = {
            date: new Date().toISOString(),
            lat: geoInfo.lat,
            long: geoInfo.long,
            city: geoInfo.city,
            region: geoInfo.region,
            country: geoInfo.country,
            countryCode: geoInfo.countryCode,
            ip: historyModel.ip
        }

        historyModel.geolocation.push(geolocation)

        let conn = new Conector().connection(identifier)
        let data = JSON.parse(JSON.stringify(historyModel))

        await HistoryModel(conn).updateOne({
            sessionID: historyModel.sessionID
        }, {
            $set: data
        })

        return geolocation
    }
    catch (error)
    {
        console.log(error)
    }
}