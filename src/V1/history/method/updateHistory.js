
import connection from '../../../method/connection'
import HistoryModel from '../../../model/history'
import createHistory from './createHistory'

export default async ({
    sessionID,
    identifier,
    data = {}
}) => {
        let ctx = {
            params: {
                identifier: identifier
            }
        }

        let conn = await connection(ctx)
        sessionID = sessionID == "null" || sessionID == "undefined" ? null : sessionID
        data.sessionID = sessionID

        let dataSID = data.user?.sId

        let historyWithSessionID = await broker.call('v1.history.get', {
            sessionID: sessionID,
            identifier: identifier
        })

        if(dataSID)
        {
            let historyWithUser = await broker.call('v1.history.get', {
                'user.sId': dataSID,
                identifier: identifier
            })
    
            if(historyWithUser)
            {
                let dat = data

                delete dat.sessionID
                return await HistoryModel(conn).findOneAndUpdate({
                    sessionID: historyWithUser.sessionID
                }, data, {new: true})
            }
        }
        
        if(!historyWithSessionID)
            return await createHistory(data, conn, ctx)

        let historySessionSID = historyWithSessionID.user?.sId
        let canUpdate = historySessionSID == dataSID || historySessionSID == null
        let canCreate = historySessionSID && dataSID && historySessionSID != dataSID

        if(canUpdate)
        {
            return await HistoryModel(conn).findOneAndUpdate({
                sessionID: historyWithSessionID.sessionID
            }, data, {new: true})
        }
        else if(canCreate)
        {
            delete data.sessionID
            return await createHistory(data, conn, ctx)
        }

        return historyWithSessionID
    }