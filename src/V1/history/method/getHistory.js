
import HistoryModel from '../../../model/history'

export default async (data, conn, ctx) => {
    delete data.identifier
    
    let history = await HistoryModel(conn).findOne(data);
    return history
}
