
export default async (ip, historyModel) => {

    if(!ip)
        return historyModel
    
    historyModel.ip = ip
    await historyModel.save()

    return historyModel
}