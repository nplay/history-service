
const fetch = require('node-fetch');

import HistoryModel from '../../../model/history'
import Conector from '../../../conector';
import geolocation from '../method/geolocation'

let findHistoryWeather = async (conn, sessionId, dateGte, ip) => {
    let result = await HistoryModel(conn).aggregate([
        { $unwind: "$weather" },
    
        {
            $match: {
                sessionID: sessionId,
                "weather.date": { $gte: dateGte },
                "weather.ip": ip
            }
        },
        
        {
            $sort: { "weather.date": -1 }
        },
        
        {
            $group: { _id: "$sessionID", weather: { $first: "$weather" } }
        }
    ])

    return result.length == 0 ? null : result[0].weather
}

export default async ({
    historyModel,
    identifier
}) => {
    try
    {
        let conn = new Conector().connection(identifier)

        let dateValid = new Date()
        dateValid.setHours(dateValid.getHours() - 6)

        let historyWeather = await findHistoryWeather(conn, historyModel.sessionID, dateValid, historyModel.ip)
        if(historyWeather)
            return historyWeather;

        let historyGeolo = await geolocation({
            historyModel: historyModel,
            identifier: identifier
        })

        if(!historyGeolo)
            throw new Error(`Geolocation not found impossible continue`)

        //Faz rotina de busca
        let weather = await broker.call('v1.weather.byRegion', {
            region: {
                city: historyGeolo.city,
                state: historyGeolo.region,
                countryCode: historyGeolo.countryCode
            },
            gteDate: dateValid.toString()
        })

        historyWeather = Object.assign({
            ip: historyModel.ip
        }, weather.toObject())

        historyModel.weather.push(historyWeather)

        await HistoryModel(conn).updateOne({
            sessionID: historyModel.sessionID
        }, {
            $set: {
                weather: historyModel.weather
            }
        })

        return weather
    }
    catch (error)
    {
        console.log(error)
    }
}