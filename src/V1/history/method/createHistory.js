
import HistoryModel from '../../../model/history'

export default async (data, conn, ctx) => {

    let identifier = ctx.params.identifier

    let historyData = Object.assign(data, {
        created_at: new Date,
        sessionID: data.sessionID || `${identifier}_${new Date().getTime()}`
    })

    let historyModel = new HistoryModel(conn)(historyData);
    await historyModel.save()

    broker.emit("history.savedAfter", {
        historyModel: historyModel,
        identifier: identifier
    });
    
    return historyModel
}