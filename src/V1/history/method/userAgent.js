
export default async (userAgent, historyModel) => {

    if(!userAgent)
        return historyModel

    let lastIdx = historyModel.userAgent.length -1
    let lastUserAgent = historyModel.userAgent[lastIdx] || {}

    if(lastUserAgent.value == userAgent)
        return historyModel

    let isMobile = userAgent.match(/\smobile/i) != null

    historyModel.userAgent.push({
        value: userAgent,
        mobile: isMobile
    })

    await historyModel.save()

    return historyModel
}