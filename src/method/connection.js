import Conector from '../conector'

export default async (ctx) => {
    let identifier = ctx.params.identifier

    if(!identifier)
        throw new Error(`Identifier not found impossible connect in database`)

    return await new Conector().connect(identifier)
}