import config from "./config"

var mongoose = require("mongoose")

class Conector
{
    constructor()
    {
    }

    get instances()
    {
        if(!Conector._instances)
            Conector._instances = new Object

        return Conector._instances
    }

    connection(name)
    {
        let instances = this.instances

        if(!instances.hasOwnProperty(name))
            instances[name] = this.createConnection(name)
    
        return instances[name]
    }

    createConnection(name)
    {
        let db = config.database

        let connString = `mongodb://${db.host}:${db.port}/${name}`
        return mongoose.createConnection(connString)
    }

    connect(db)
    {
        return this.connection(db)
    }
}

export default Conector