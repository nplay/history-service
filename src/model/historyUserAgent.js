import Conector from "../conector";
var mongoose = require("mongoose")

var schema = new mongoose.Schema(
    {
        created_at: {
            type: Date,
            required: true,
            default: new Date()
        },
        value: {
            type: String,
            required: false,
            default: null
        },
        mobile: {
            type: Boolean,
            default: false
        }
    }
);

schema.query.byId = function (ids) {
};

let nameModel = "HistoryUserAgent";

class ModelClass {
}

schema.loadClass(ModelClass);

export default (connection = new Conector().connect('default')) => {
    let hasModel = connection.models.hasOwnProperty(nameModel)
    return hasModel ? connection.model(nameModel) : connection.model(nameModel, schema)
}