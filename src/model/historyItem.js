import Conector from "../conector";

import Origin from "./OriginModel"
import AccessModel from "./accessModel"

var mongoose = require("mongoose")

var schema = new mongoose.Schema(
    {
        inserted_at: {
            type: Date,
            default: new Date()
        },
        product: new mongoose.Schema({
            created_at: Date,
            id: String,
            entityId: String,
            categorys: {
                type: Array
            },
        }, { strict: false }),
        views: {
            type: Number,
            default: 1
        },
        accessCollection: [AccessModel().schema],
        originCollection: [Origin().schema]
    }
);

let nameModel = "HistoryItem";

class ModelClass {
}

schema.loadClass(ModelClass);

export default (connection = new Conector().connect('default')) => {
    let hasModel = connection.models.hasOwnProperty(nameModel)
    return hasModel ? connection.model(nameModel) : connection.model(nameModel, schema)
}