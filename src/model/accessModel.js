import Conector from "../conector";
var mongoose = require("mongoose")

var schema = new mongoose.Schema(
    {
        date: {
            type: Date,
            default: new Date()
        },
        ocurrence: {
            type: Number,
            default: 1
        }
    }
);

schema.query.byId = function (ids) {
};

let nameModel = "AccessModel";

class ModelClass {
}

schema.loadClass(ModelClass);

export default (connection = new Conector().connect('default')) => {
    let hasModel = connection.models.hasOwnProperty(nameModel)
    return hasModel ? connection.model(nameModel) : connection.model(nameModel, schema)
}