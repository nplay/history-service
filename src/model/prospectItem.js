var mongoose = require('mongoose');
import Origin from "./OriginModel"
import Conector from "../conector";
import AccessModel from "./accessModel";

var schema = new mongoose.Schema(
    {
        inserted_at: {
            type: Date,
            default: new Date()
        },
        product: new mongoose.Schema({
            created_at: Date,
            id: String,
            entityId: String,
            categorys: {
                type: Array
            }
        }, { strict: false }),
        orderId: {
            type: String,
            default: null
        },
        views: {
            type: Number,
            default: 1
        },
        accessCollection: [AccessModel().schema],
        originCollection: [Origin().schema]
    }
);

schema.query.byId = function(ids){
};

let nameModel = "HistoryProspectItem";

class ModelClass
{
}

schema.loadClass(ModelClass);

export default (connection = new Conector().connect('default')) => {
    let hasModel = connection.models.hasOwnProperty(nameModel)
    return hasModel ? connection.model(nameModel) : connection.model(nameModel, schema)
}