
var mongoose = require("mongoose")

import HistoryItem from './historyItem'
import ProspectItem from './prospectItem'
import HistoryGeolocation from './historyGeolocation'
import HistoryWeather from './historyWeather'
import HistoryUser from './historyUser'
import HistoryUserAgent from './historyUserAgent'
import Conector from '../conector'

var schema = new mongoose.Schema(
    {
        created_at: Date,
        sessionID: {
            type: String,
            required: true
        },
        user: HistoryUser().schema,
        userAgent: [HistoryUserAgent().schema],
        itens: [HistoryItem().schema],
        prospectItens: [ProspectItem().schema],
        geolocation: [HistoryGeolocation().schema],
        weather: [HistoryWeather().schema],
        ip: {
            type: String,
            required: false,
            default: null
        }
    }
);

class HistoryModelClass
{
    static async bestViewed(categoryIds, itensId)
    {
        let queryArray = [
            {
                "$unwind": "$itens"
            },
            {
                "$group": {
                    _id: "$itens.id",
                    ocurrences: { "$sum": "$itens.views" },
                    categorys: { "$first": "$itens.details.categorys" }
                }
            },
            {
                "$match": {
                    _id: { $in: itensId || new Array }
                }
            }
        ]

        if(categoryIds != undefined && categoryIds != null)
        {
            queryArray.push({
                "$match": {
                    "categorys": {
                        "$in": categoryIds
                    }
                }
           })
        }

        let response = await this.aggregate(queryArray)
        let bestViewedCategorys = new Object()

        for(let match of response)
        {
            for(let catId of match.categorys)
            {
                if(bestViewedCategorys.hasOwnProperty(catId) == false)
                    bestViewedCategorys[catId] = 0
                
                bestViewedCategorys[catId] += match.ocurrences
            }
        }

        return bestViewedCategorys
    }
}

let nameModel = "History";
schema.loadClass(HistoryModelClass);

export default (connection = new Conector().connect('default')) => {
    let hasModel = connection.models.hasOwnProperty(nameModel)
    return hasModel ? connection.model(nameModel) : connection.model(nameModel, schema)
}