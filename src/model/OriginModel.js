import Conector from '../conector';
var mongoose = require('mongoose');

var schema = new mongoose.Schema(
    {
        id: {
            type: String,
            required: true
        },
        title: {
            type: String,
            required: true
        },
        node: {
            id: {
                type: String,
                required: true
            },
            name: {
                type: String,
                required: true
            }
        }
    }
);

let nameModel = "HistoryProspectItemOrigin";

class ModelClass
{
}

schema.loadClass(ModelClass);

export default (connection = new Conector().connect('default')) => {
    let hasModel = connection.models.hasOwnProperty(nameModel)
    return hasModel ? connection.model(nameModel) : connection.model(nameModel, schema)
}