
var mongoose = require("mongoose")

import Conector from '../conector'

var schema = new mongoose.Schema(
    {
        created_at: {
            type: Date,
            default: Date.now()
        },
        ip: String,
        country: String,
        countryCode: String,
        city: String,
        region: String,
        lat: String,
        long: String
    }
);

class Class
{
}

let nameModel = "Geolocation";
schema.loadClass(Class);

export default (connection = new Conector().connect('default')) => {
    let hasModel = connection.models.hasOwnProperty(nameModel)
    return hasModel ? connection.model(nameModel) : connection.model(nameModel, schema)
}