import Conector from "../conector";

var mongoose = require("mongoose")

var schema = new mongoose.Schema(
    {
        date: Date,
        city: String,
        region: String,
        countryCode: String,
        temp: Number,
        climate: String
    }
);

class WeatherClass
{
    
}

let nameModel = "Weather";
schema.loadClass(WeatherClass);

export default (connection = new Conector().connect('default')) => {
    let hasModel = connection.models.hasOwnProperty(nameModel)
    return hasModel ? connection.model(nameModel) : connection.model(nameModel, schema)
}