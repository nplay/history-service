import Conector from "../conector";
var mongoose = require("mongoose")

var schema = new mongoose.Schema(
    {
        sId: {
            type: String,
            required: true
        },
        name: {
            type: String
        },
        email: {
            type: String
        }
    }
);

schema.query.byId = function (ids) {
};

let nameModel = "HistoryUser";

class ModelClass {
}

schema.loadClass(ModelClass);

export default (connection = new Conector().connect('default')) => {
    let hasModel = connection.models.hasOwnProperty(nameModel)
    return hasModel ? connection.model(nameModel) : connection.model(nameModel, schema)
}