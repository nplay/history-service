import Conector from "../conector";
var mongoose = require("mongoose")

var schema = new mongoose.Schema(
    {
        date: Date,
        lat: String,
        long: String,
        city: String,
        region: String,
        country: String,
        countryCode: String,
        ip: String
    }
);

schema.query.byId = function (ids) {
};

let nameModel = "HistoryGeolocation";

class ModelClass {
}

schema.loadClass(ModelClass);

export default (connection = new Conector().connect('default')) => {
    let hasModel = connection.models.hasOwnProperty(nameModel)
    return hasModel ? connection.model(nameModel) : connection.model(nameModel, schema)
}